Subject: Employment & Payroll Forms for GitLab GmbH

Hi PERSON,

We are so excited to have you join GitLab! So that we can add you to our payroll system as quickly as possible, please complete the attached `GTL008_Monthly_LPI_Template_Germany` form as soon as possible. The columns highlighted in red are the mandatory columns for you to fill in. If any other columns are relevant to you, please feel free to complete but the rest will be completed by GitLab, as needed.

Once completed, please send a reply back to notify us accordingly. 

Let us know if you have any questions.

Thank you!
