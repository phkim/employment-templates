# Temporary Deactivation - Interns

This is being created as a way to track the temporary deactivation of systems for interns that have completed their internship, have been employed as full time team members, and there is a gap in the employment.

This should not be considered a full offboarding of the team member.

## **Completing Deactivation Issue**

To ensure a successful completion of the issue, it is important that all tasks are checked off, whether the task is applicable to the offboarding team member or not. Checking the box indicates one of the following:

- I have completed this task
- I have checked and this task is not applicable to me

## **Compliance**

**It is important to note that all deactivation tasks by all Departments need to be completed within 5 days of the offboarding date. For systems that are more critical and time sensitive, these will be completed within the first 24 hours (example 1Password, Okta, Slack) by the relevant Departments.**


|                   |                     |
| ----------------- | ------------------- |
| Entity            | `__ENTITY__`        |
| GitLab Department | `__DEPARTMENT__`    |
| GitLab E-mail     | `__GITLAB_EMAIL__`  |
| GitLab.com Handle | `__GITLAB_HANDLE__` |



<details>
<summary>Manager</summary>

1. [ ] Manager: Review all open merge requests and issues assigned to the team member and reassign them to you or another team member as per your handover agreement within the team.
1. [ ] Coordinate with IT Ops @gitlab-com/business-technology/team-member-enablement to change any shared passwords, with the intent to be gated behind Okta when possible, in particular;
   1. [ ] Review if team member had sysadmin access passwords for GitLab.com Infrastructure (ssh, chef user/key, others). Identify if any can be moved to Okta.
1. [ ] Organize a smooth hand over of any work or tasks from the interns.
1. [ ] Scroll to the bottom of this issue; depending on the department, division, or entity there may be additional manager tasks.

</details>

## People

<details>
<summary>People Connect</summary>

### Complete the below tasks ASAP

1. [ ] People Connect: Once the internship has completed, as soon as possible, create a **confidential** issue called 'Temporary Deactivation (NAME), per (DATE, please follow yyyy-mm-dd)' in in the [People Ops Employment Issue Tracker](https://gitlab.com/gitlab-com/team-member-epics/employment/issues) with relevant lines of the master offboarding checklist.
1. [ ] People Connect: For this deactivation, the manager is `__MANAGER_HANDLE__`, and People Connect is handled by `__PEOPLE_EXPERIENCE_HANDLE__`. Assign this issue to them.
1. [ ] People Connect: Enter the former team member's GitLab email address, handle, and Department above.

### BambooHR

1. [ ] At the far right corner of the team member's profile, click on the gear icon, choose `Terminate Employee`. Use the date mentioned in this issue as final date of employment / contract. Enter Termination Type, Termination Reason and Eligible for Rehire per the answers received from the People Partner.
1. [ ] Okta entitlements are driven based on BambooHR status. Therefore to remove the user in Okta, the best way to set off that workflow is to force an Import from BambooHR into Okta. To do this, log into the [Okta dashboard](https://gitlab-admin.okta.com/) and select `Admin` in the upper-right hand side of the page. Once on the Okta Admin dashboard, select `Applications`, search and select `BambooHR Admin`, go to the `Import` tab and click the `Import Now` button. This will force an import, and you should see a message that a user has been removed after processing completes.

_Note: This will also trigger any deprovisioning workflows for Applications that Okta is configured to perform Deprovisioning on, as well as disable the user's Okta account._

### Notifications

** Please ensure to mention that this is a temporary deactivation of the accounts until the team member starts again with GitLab as a fully time employee.**

1. [ ] Notify Security of the offboarding (@JohnathanHunt).
1. [ ] Notify CFO (@brobins) and Sr Stock Administrator (@tdominique) of offboarding.
1. [ ] Depending on the team members location, please ping the relevant payroll contact persons in this issue.
         - US and Canada: Cristine Marquardt and Javonna Russell
         - Non-US: Sylwia Szepietowska and Nicole Precilla
</details>


## Business Technology

<details>
<summary>Team Member Enablement (IT)</summary>

1. [ ] IT Ops: Log on to the [Google Admin console](https://admin.google.com/gitlab.com/AdminHome?pli=1&fral=1#UserList:org=49bxu3w3zb11yx) and find the team member's profile.
1. [ ] IT Ops: Click the `More` button (left hand side of screen) and then click `Suspend User` and click it again to confirm. This will ensure that the user can no longer access their account.
1. [ ] IT Ops: Deactivate the user from all groups they are associated with.
1. [ ] IT Ops: Following the [instructions in the Handbook](https://about.gitlab.com/handbook/people-group/offboarding/offboarding_guidelines/#g-suite), set an automatic rejection rule. This sends an auto-response to the sender notifying them that the team member is no longer with GitLab and who to contact instead.
1. [ ] IT Ops @gitlab-com/business-technology/team-member-enablement : Deactivate Interns GitLab.com account from the [gitlab-com group](https://gitlab.com/groups/gitlab-com/-/group_members).
   1. [ ] IT Ops @gitlab-com/business-technology/team-member-enablement : Temporarily block the Intern, remove from all company Groups and Projects, and remove GitLab job role if visible on profile.
   1. [ ] IT Ops @gitlab-com/business-technology/team-member-enablement : Deactivate the Interns admin account, if applicable.
1. [ ] IT Ops @gitlab-com/business-technology/team-member-enablement : Deactivate former GitLab Team Member's [dev.GitLab.org account](https://dev.gitlab.org/admin/users) and remove from [gitlab group](https://dev.gitlab.org/groups/gitlab/group_members).
1. [ ] IT Ops @gitlab-com/business-technology/team-member-enablement : Deactivate former GitLab Team Member's [staging.GitLab.com account](https://staging.gitlab.com/admin/users) and [gitlab group](https://staging.gitlab.com/groups/gitlab/group_members).
1. [ ] IT Ops: [Check if the team member has created any Slack bots](https://about.gitlab.com/handbook/people-group/offboarding/offboarding_guidelines/) before disabling account. [Link](https://gitlab.slack.com/apps/manage?utm_source=in-prod&utm_medium=inprod-apps_link-slack_menu-click) to Apps menu.
1. [ ] IT Ops: Temporarily disable the Intern in [Slack](https://gitlab.slack.com/admin).
1. [ ] IT Ops: Log into [1Password](https://1password.com/) and click on "People" in the right-hand sidebar. Search for the team member's name. Click "More Actions" under their name and choose "Suspend" to remove access to 1Password. Take a screenshot of the user's permissions and post it as a comment in this offboarding issue.
1. [ ] IT Ops: Okta should now deprovision Zoom as part of Okta Deactivation, please verify it was deactivated. If not, please ping the team in the #it-help Slack channel.


</details>

## Finance

<details>
<summary>Accounting</summary>

1. [ ] Accounting (@Courtney_cote @kayokocooper): Cancel company American Express card if applicable.
1. [ ] Accounting (@Courtney_cote @kayokocooper): Remove team member from Expensify.


</details>

# Tech Stack System Deprovisioning / Offboarding - Tasks that need to be completed within 5 days

### System Admins


Review the table of applications listed below that is included in our [Tech Stack](https://docs.google.com/spreadsheets/d/1mTNZHsK3TWzQdeFqkITKA0pHADjuurv37XMuHv12hDU/edit#gid=0). For applications that you Own/Admin, please review and ensure the former team member does **not** have access to your system(s).

**If the user DOES have access to a system you administer access to:**

- Review and remove the former team member's access.
- Select the applicable checkbox(es) once access has been removed from the impacted system.

**If the user DOES NOT have access to a system you administer access to:**

- After confirming the former team member does not have access to the system, select the checkbox indicating access has been deprovisioned.


### Data

<summary>Taylor Murphy @tayloramurphy , Justin Stark @jjstark</summary>

1. [ ] Snowflake
1. [ ] Stitch
1. [ ] Fivetran

### Development

<summary>Lukas Eipert @leipert </summary>

1. [ ] JetBrains - Revoke JetBrains licenses. Go to the [user management](https://account.jetbrains.com/organization/3530772/users) and search for the team member, revoke their licenses.

### Team member Enablement (IT)

<summary> Mohammed Al Kobaisy @malkobaisy </summary>

1. [ ] PagerDuty

<summary>Marc DiSabatino @marc_disabatino </summary>

1. [ ] WebEx

<summary> @gitlab-com/business-technology/team-member-enablement </summary>

1. [ ] LucidChart

### Security

<summary>Jan Urbanc @jurbanc </summary>

1. [ ] Tenable.IO
1. [ ] Rackspace (Security Enclave)

<summary>Andrew Kelly @ankelly, Dominic Couture @dcouture</summary>

1. [ ] HackerOne




---

# Job Specific Offboarding Task
If a task only applies to a Entity, Country, Division, Department or Role it will appear under here.

<!-- include: entity_tasks -->

---

<!-- include: country_tasks -->

---

<!-- include: division_tasks -->

---

<!-- include: department_tasks -->

---

<!-- include: people_manager_tasks -->
