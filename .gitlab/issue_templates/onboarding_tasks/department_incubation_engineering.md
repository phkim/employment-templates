#### Engineering Division - Development/Incubation

<!-- Note: If you make any changes to this section, please also update `department_development.md`. -->

<details>
<summary>New Team Member</summary>

1. [ ] Become familiar with GitLab. Familiarize yourself with the dashboard, the projects, and the issue tracker. Become familiar with the `README.md`s for these projects:
   * [GitLab www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/README.md)
   * [GitLab](https://gitlab.com/gitlab-org/gitlab/blob/master/README.md)
   * [GitLab FOSS Edition](https://gitlab.com/gitlab-org/gitlab-foss/blob/master/README.md).
**Note:** that the Gitlab FOSS (Free Open Source Software) Edition is a read-only mirror of GitLab with the proprietary software removed. [This issue](https://gitlab.com/gitlab-org/gitlab/issues/13304) covers the merging of the Enterprise and Community editions.
1. [ ] Refer to [GitLab Docs](https://docs.gitlab.com/ee/README.html) for information on features of GitLab.
1. [ ] Download and [get started with Git](https://docs.gitlab.com/ce/gitlab-basics/start-using-git.html).  It is important for all team members to understand how to work both on the web interface and locally. The team is happy to assist in teaching you git.
1. [ ] Consider subscribing to [status.gitlab.com](https://status.gitlab.com) to stay aware of active incidents.

1. [ ] Read the [Developer Onboarding](https://about.gitlab.com/handbook/developer-onboarding/)
1. [ ] Familiarize yourself with the [engineering handbook](https://about.gitlab.com/handbook/engineering) and relevant pages linked from there, including the [development process for Security Releases](https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/security/developer.md).
1. [ ] If you need to run the GitLab EE edition on your local environment, [request a GitLab EE developer license](https://about.gitlab.com/handbook/developer-onboarding/#working-on-gitlab-ee-developer-licenses) and configure it following the steps described on the [documentation page](https://docs.gitlab.com/ee/user/admin_area/license_file.html).
1. [ ] If you want [U2F security](https://about.gitlab.com/handbook/tools-and-tips/#u2f), purchase and expense for yourself a [YubiKey 5 Series](https://www.yubico.com/products/yubikey-5-overview/) or greater.
1. [ ] Set a reminder to add yourself as a [trainee maintainer](https://about.gitlab.com/handbook/engineering/workflow/code-review/#trainee-maintainer) (Senior Engineer+) or code reviewer (Intermediate Engineer) for GitLab after working here for 3 months. You can do this by adding the appropriate entries to the [team page entry file](https://gitlab.com/gitlab-com/www-gitlab-com/-/tree/master/data/team_members/person) file with which you added yourself to the team page. Here are [examples for frontend](https://gitlab.com/search?search=%22gitlab%3A+reviewer+frontend%22&nav_source=navbar&project_id=7764&group_id=6543&search_code=true&repository_ref=master), and [examples for backend](https://gitlab.com/search?search=%22gitlab%3A+reviewer+backend%22&nav_source=navbar&project_id=7764&group_id=6543&search_code=true&repository_ref=master). Doing reviews is a good way to help other team members, improve GitLab, and learn more about the code base. Because we use [reviewer roulette](https://docs.gitlab.com/ee/development/code_review.html#reviewer-roulette) once you add yourself as a reviewer, people will assign merge requests for you to review.
1. [ ] Ensure that you've [requested to be added to chatops for Gitlab.com administrative tasks](https://docs.gitlab.com/ee/development/chatops_on_gitlabcom.html#requesting-access).
1. [ ] Try connecting to the [staging rails console via Teleport](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/Teleport/Connect_to_Rails_Console_via_Teleport.md). This can be helpful for verifying fixes and troubleshooting issues.
1. [ ] Review the [tech stack YAML file](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/tech_stack.yml) to see what systems are available to you, or other team members that can help you use them.
1. [ ] Create an [Iteration Training](https://gitlab.com/gitlab-com/Product/-/issues/new?issuable_template=iteration-training) issue, assign it yourself, and complete the training. 
1. [ ] Create a [Feature Flag Training](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/new?issuable_template=feature-flag-training) issue, assign it yourself, and complete the training. 
1. [ ] Complete the [Reliability Training in GitLab Level Up](https://levelup.gitlab.com/courses/development-reliability).
1. [ ] Read the handbook page on [Psychological Safety](https://about.gitlab.com/handbook/leadership/emotional-intelligence/psychological-safety/).
1. [ ] Plan to watch the [secure coding training videos](https://about.gitlab.com/handbook/engineering/security/secure-coding-training.html).  Note that they were recorded over two days.  It is suggested you break this up by topic and/or by hour over the next couple weeks.  They cover secure coding practices in general and also cover security risks and mitigations for Ruby on Rails applications.

</details>


<details>
<summary>Manager</summary>

1. [ ] If [baseline entitlements](https://gitlab.com/gitlab-com/team-member-epics/access-requests/tree/master/.gitlab/issue_templates) exist for the new team member's role, create or add to an already created access request using the appropriate template. (This may also cover some other items on this list.). If the role exists in [the access request repo](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/tree/master/.gitlab/issue_templates/role_baseline_access_request_tasks) you can skip this task.
1. [ ] Determine if new team member will need access to the [Staging server](https://about.gitlab.com/handbook/engineering/infrastructure/environments/#staging), which is used by engineers to test their changes on a Production-like environment before they land on Production. If so, create or add to an already created [access request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/issues) *with the same username the team member has on gitlab.com*.
1. [ ] Let the new team member know their account on staging.gitlab.com has been created with the same username that they have on gitlab.com.

</details>

#### Incubation Engineering Department

<details>
<summary>New Team Member</summary>

##### Access requests

1. [ ] Request "Create Dashboard" Permissions in Sisense: Open an [access request issue](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new), select `Individual_bulk_access_request` from the list of templates and add your details. Follow the [Access Request Steps](https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/access-requests/#how-do-i-choose-which-template-to-use) when creating the issue. See an [example issue here](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/16040)
2. [ ] Request "License" to use/contribute to EE features locally. Open an [access request issue](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=GitLab_Team_Member_License_request&_gl=1*1f10mxu*_ga*MjIyOTkxMDU5LjE2NjI5NTk5NzA.*_ga_ENFH3X7M5Y*MTY2NDQyOTIxOC40OS4xLjE2NjQ0MzE4MzguMC4wLjA.), add `Developer on-boarding` as reason.

##### Create your SEG

1. [ ] Make yourself familiar with the [Incubation Engineering Playbook](https://about.gitlab.com/handbook/engineering/incubation/playbook/#incubation-engineers-playbook)
1. [ ] Follow all steps to [Convert an incubation backlog project to an active SEG](https://about.gitlab.com/handbook/engineering/incubation/playbook/#converting-an-incubation-backlog-project-to-an-active-seg)

</details>
