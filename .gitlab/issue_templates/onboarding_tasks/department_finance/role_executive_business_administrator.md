#### Executive Business Administrator Tasks

<details>
<summary>New Team Member</summary>

1. [ ] Review the [Executive Business Administrators Handbook Page](https://about.gitlab.com/handbook/eba/)
1. [ ] Review the [Company Expense Handbook Page](https://about.gitlab.com/handbook/finance/expenses/)
1. [ ] Get [Scheduling Privileges for Zoom](https://support.zoom.us/hc/en-us/articles/201362803-Scheduling-privilege) from your Executives
1. [ ] Get Copilot Access to Expensify from your Executives - instructions [here](https://community.expensify.com/discussion/4783/how-to-add-or-remove-a-copilot)
1. [ ] Get [Delegate Access to TripActions](https://community.tripactions.com/s/article/delegate-self-serve#:~:text=Designating%20Delegate%20Access%20to%20Another%20Traveler&text=the%20below%20steps%3A-,Log%20in%20and%20navigate%20to%20your%20Profile%20from%20the%20TripActions,to%20give%20delegate%20permissions%20to.) from your Executives
1. [ ] Get Calendar Access from your Executives -instructions [here](https://support.google.com/calendar/answer/37082?hl=en)
1. [ ] Install the [Chrome Zoom Plugin](https://chrome.google.com/webstore/detail/zoom-scheduler/kgjfgplpablkjnlkjmjdecgdpfankdle?hl=en)
1. [ ] Bookmark the [EBA Drive](https://drive.google.com/drive/u/0/folders/0AOGOTUVPubCmUk9PVA)
1. [ ] Work with your e-group EBA to be added to necessary Google Groups, slack channels
1. [ ] Understanding a [Key Review](https://about.gitlab.com/handbook/key-review/), [Group Conversation] (https://about.gitlab.com/handbook/group-conversations/) and your role in owning these calls for your functional area.
    1. [ ] Understand how to [live stream on Youtube](https://about.gitlab.com/handbook/marketing/marketing-operations/youtube/)!  Link to tutorial [here](https://about.gitlab.com/handbook/marketing/marketing-operations/youtube/#video-walkthough-of-how-to-livestream-to-youtube).
1. [ ] Access Requests
    1.  [ ] Coupa account access and bulk access to Coupa (similar to this [issue](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/16195#note_1000024700))
    1. [ ] Request corporate card be issued (similar to this [issue](https://gitlab.com/gitlab-com/Finance-Division/finance/-/issues/5072))
1. [ ] Review helpful Coupa Tutorials below
    1. [ ] [How to request a Virtual Card in Coupa](https://docs.google.com/document/d/13oIHwk9543s5rOj6eA2XSOyMIYFpn__TgOPMdU8jhp0/edit?usp=sharing)
    1. [ ] [How to create a purchase order](https://docs.google.com/document/d/16D3hRxzkmbsGb8DmeCOSocYHaocTgCb0_R4LVs-YvhY/edit)

</details>
