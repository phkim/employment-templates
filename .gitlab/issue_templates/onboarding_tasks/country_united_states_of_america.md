### Day 1 - For New Team Members in the US 

<details>
<summary>New Team Member</summary>

1. [ ] New team member: Ensure Section 1 of your I-9 has been completed in the LawLogix Portal you will have received instructions via your personal email account prior to your start date if you have any difficulty with this process please reach out to people-connect@gitlab.com.
1. [ ] New team member: Please review our [Disability Inclusion](https://about.gitlab.com/company/culture/inclusion/#disability-inclusion) policy.
    1. [ ] Please read and review the US Dept of Labor's OFCCP [Voluntary Self-Identification of Disability Form](https://gitlab.com/gitlab-com/people-group/people-operations/employment-templates/-/blob/main/.gitlab/issue_templates/onboarding_tasks/documents/503_Voluntary_Self-ID_Disability_Form.pdf).
1. [ ] New team member: Please review our [US Veteran Inclusion](https://about.gitlab.com/company/culture/inclusion/#united-states-veteran-inclusion) policy.
1. [ ] New team member: Please review our [People Policies - GitLab Inc (USA)](https://about.gitlab.com/handbook/people-policies/inc-usa/) page - you will acknowledge some of these within your Workday Onboarding Tasks.
1. [ ] **Colorado** residents only:  In accordance to Colorado Revised Statutes part 4 Sec. 13.3 of section 8, please read the required [CO Paid Leave](https://famli.colorado.gov/sites/famli/files/FAMLI%20Break%20Room%20Poster%20Officialv3.pdf) notice.
1. [ ] **Conneticut** residents only:  In accordance to Conn. Gen. Stat. Sec.31-49q, please read the required [CT Paid Leave](https://portal.ct.gov/-/media/DOLUI/NEW-53122-Prototype-of-Employers-Written-Notice-to-Employees-of-Rights-under-CTFMLA-and-CTPL.pdf) notice.
1. [ ] **Delaware, Minnesota, or Oregon** residents only: If you wish to opt out of our paperless electronic pay statements, please send an email to `uspayroll@gitlab.com`.
1. [ ] **Hawaii** residents only: We offer an electronic paperless pay system, by checking this box you give consent to receive your pay statements electronically. If you do not consent to electronic pay statements, please email `uspayroll@gitlab.com`.
1. [ ] **Illinois** residents only: In accordance to the Illinois Consumer Coverage Disclosure Act, team members can find the current Benefits Disclosure Summary and Summary Plan Descriptions in [the 2022 Plan Year section of the US Benefits Handbook Page](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/inc-benefits-us/#2022-plan-year). Checking this box is your confirmation of acknowledgement that you have received this disclosure. 
1. [ ] **Massachussetts** residents only: In accordance to MA [M.G.L.c. 175M](https://malegislature.gov/Laws/GeneralLaws/PartI/TitleXXII/Chapter175M), please read the required [MA Paid Family and Medical Leave](https://www.mass.gov/doc/2022-paid-family-and-medical-leave-mandatory-workplace-poster/download) notice, and review [GitLab's MA PFML Notice](https://na3.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=679902ee-1c9f-4a5a-9463-3611b8b1716f&env=na3&acct=e71baeae-8bee-495e-a89a-c62c85075891&v=2). 
    - The [GitLab's MA PFML Notice](https://na3.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=679902ee-1c9f-4a5a-9463-3611b8b1716f&env=na3&acct=e71baeae-8bee-495e-a89a-c62c85075891&v=2) link will take you to a screen to enter your full legal name and GitLab email.
    - Review GitLab's MA PFML Notice and choose to either acknowledge or decline, by checking the applicable box and entering your electronic signature.
    - Please **download your signed document** once you are done.
    - Please upload your signed document to Workday by following the related [How to: Upload Signed Documents Job Aid](https://docs.google.com/document/d/1FDUdF9AxiRGHh8v9k1zkVL_5-kWUR9JGgv4AfdyvGeU/edit).
1. [ ] **Oregon** residents only:  In accordance to ORS 659A.150 - 659A.186, please read the required [OR Paid Leave](https://paidleave.oregon.gov/DocumentsForms/Paid-Leave-ModelNotice-Poster-EN.pdf) notice.

**To Do after I-9 is complete:**
1. [ ] New team member: **(PAYROLL)** Access to ADP is granted once all of your Onboarding Tasks within Workday have been completed, you will then be able to access the platform via the ADP tile within Okta. **Please allow at least 24-48 hours for the sync between Workday and ADP to happen, though it can take up to a week. You will receive an email from ADP when your account is available.** Be sure to update ADP with all of the pertinent information including Direct Deposit and Bank Details, W4 Withholdings, Marital Status, etc. You are free to download the ADP Mobile Solutions app using our [instructions](https://about.gitlab.com/handbook/finance/non-exempt/#adp-mobile-solutions).
1. [ ] New team member: **(BENEFITS)** Your invitation to PlanSource will arrive between Day 4-7. Once you receive your invitation, elect your benefits through the PlanSource platform. Your benefit choices will be retroactive to your start date, but [it may take a few weeks to receive ID cards and policy details](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/inc-benefits-us/#id-cards). If you have questions, please contact the Willis Towers Watson Benefits Support Helpline at 877-409-9288 or GitLab@willistowerswatson.com.
    1. [ ] New team member: Please ensure you also elect in to your 2023 benefit plans in addition to your 2022 benefit plans, as GitLab's open enrollment for the 2022 year starts on October 31, 2022. You should be prompted to start your 2023 Annual Enrollment in PlanSource after you finish your New Hire Enrollment.
    1. [ ] **Note:** Benefit files are sent to Cigna from Plansource weekly on **Wednesdays**. Example of timing: you start on a Monday, you'll receive Plansource enrollment email on Thursday. If you make elections by the following Tuesday, your plans will go over to Cigna on Wednesday and your myCigna account will be created by Friday. 
    1. [ ] New team member: For more information on our 2022 & 2023 plans, please review the [handbook](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/inc-benefits-us/) and [Summary Plan Description](https://drive.google.com/file/d/1K8uybZ_pQc-XxpOaIEqnSN-UlvZCsf1W/view?usp=sharing).

1. [ ] New team member: Review [GitLab's 401(k)](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/inc-benefits-us/#401k-plan) Plan. You will receive an email from Betterment within your first week, usually between Day 4-7. If you do not, please reach out to [People Connect](https://about.gitlab.com/handbook/people-group/people-connect/). NOTE: if you've had a Betterment account in the past and haven't received an email after Day 7, login to Betterment with your prior credentials, the plan may already be set up there. 
   1. New team member: For more information on the plan, please review the [Summary Plan Description](https://drive.google.com/file/d/1k1xWqZ-2HjOWLv_oFCcyBkTjGnuv1l_B/view?usp=sharing) and [Fee Disclosure](https://drive.google.com/file/d/1gWxCI4XI01gofUu9yqKd4QyXeM27Xv57/view?usp=sharing).
   1. **Important Note**:  ADP payroll system monitors and will stop the 401(k) contribution when you have reached the IRS limit for the year, but please keep in mind if you have made prior contributions with another 401(k) plan, ADP will not have access to this information. If you wish to max out your 401(k) please be sure to subtract any employee only contributions you had already made under a prior plan, from the current year's [IRS 401k annual limit](https://www.irs.gov/retirement-plans/plan-participant-employee/retirement-topics-401k-and-profit-sharing-plan-contribution-limits), divide that number by the remaining amount of paychecks you will receive for the year using the [payroll calendar](https://about.gitlab.com/handbook/finance/payroll/#payroll-cut-off-dates), and contribute that dollar amount per paycheck. 
1. [ ] New team member: **Optional:** Join the `#loc_usa` Slack channel in order to receive USA-specific announcements.
   - **Important Note:** This slack channel is a social and announcement channel so it can be overwhelming. All important announcements that require action will also be sent via e-mail and cross posted in the `#whats-happening-at-gitlab` slack channel if you decide not to join.

</details>

<details>
<summary>People Connect</summary>

### Prior to Start Date
1. [ ] People Connect: The Workday/LawLogix sync will ensure soon to onboard team members receive their I-9 completion Instruction email at least seven days prior to their start date. 
1. [ ] People Connect: Lawlogix will automatically send the invite to the new team member's Designated Agent for Section 2 to be completed.
1. [ ] People Connect: Run the completed I-9 through E-Verify within the LawLogix Guardian platform. 
1. [ ] People Connect: After the case has been submitted to E-Verify, check to see whether employment has been authorized or whether any further action is needed. Steps for photo matching can be found in the [Handbook](https://about.gitlab.com/handbook/people-group/general-onboarding/onboarding-processes/#e-verify-photo-matching).
1. [ ] People Connect: If the team member is a rehire, immediately alert Total Rewards for a resync of benefits.

</details>

<details>
<summary>Payroll</summary>

1. [ ] Payroll @Ngundara, @ybasha: Add new team member to ADP and send invitation to team member once the I-9 process has been confirmed as complete. 
1. [ ] Payroll @Ngundara, @ybasha: Communicate to the People Connect Team that the team member has been successfully added.
1. [ ] Payroll @Ngundara, @ybasha: If team member is a resident of **Hawaii** confirm that they have checked the box above giving their consent to receive electronic pay statements escalating if necessary.

</details>

