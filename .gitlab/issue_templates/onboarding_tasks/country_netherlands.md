### For New Team Members in the Netherlands 

<details>
<summary>New Team Member</summary>

1. [ ] New Team Member: Confirm in this issue with a comment that you completed the payroll form for HR Savvy during your offer signing process. This info is incredibly important to ensure you are uploaded to payroll.
1. [ ] New Team Member: HR Savvy will initiate the process surrounding your application for a [Certificate of Good Conduct or VOG (Verklaring Omtrent het Gedrag)](https://internal-handbook.gitlab.io/handbook/people-group/people-operations/people-connect/frequently_requested/#netherlands-certificate-of-good-conduct). (Log into Okta and click on the GitLab Internal Hanbook tile to access this link). Please be sure to upload this document to Workday you can do so by navigating to your `Workday Profile` via your image in the upper right hand corner of the screen and selecting `Personal` followed by the `Documents Tab` once you have done so be sure to mark this task as complete.  **IMPORTANT** This must be done within **thirty (30) days** of your start date.
1. [ ] New Team Member: Confirm with a comment in this issue that you completed the [wage tax form](https://drive.google.com/a/gitlab.com/file/d/1q8N-idoYGFSCw2ajat9RscfQ004EL-PS/view?usp=sharing).
1. [ ] New Team Member: If you don't have a BSN number you will need to apply for one asap. Details on that process can be found under the BSN number section on the [visas page in the handbook](https://about.gitlab.com/handbook/people-operations/visas/#bsn-number). If you are already informed of this by HRSavvy, please comment in this issue confirming this. 
1. [ ] New Team Member: To get your internet subscription [reimbursed](https://about.gitlab.com/handbook/finance/expenses/#approved-categories-for-reimbursement) (point 2), fill in and sign the [Regeling Internet Thuis form](https://docs.google.com/a/gitlab.com/document/d/1J70geARMCjRt_SfxIY6spdfpTbv_1v_KDeJtGRQ6JmM/edit#heading=h.5x5ssjstqpkq) and email it to HR Savvy. **Please do not submit the internet reimbursement via Expensify or TripActions - these will be rejected**
1. [ ] New Team Member: For any future questions/concerns regarding your employment with GitLab, these should be directed to the People Connect team at GitLab (not HR Savvy). We will be able to help support in getting these answered/resolved. Any payroll related questions can be directed to the Payroll team via `nonuspayroll@gitlab.com`.

</details>

<details>
<summary>People Connect</summary>

1. [ ] People Connect: Ensure that the team members contract particulars have been captured in Workday. You can add the required information by following [this Job Aid](https://docs.google.com/document/d/1qnRUH2vkBGDi1hWnpGjvfF0G1GvfZLVHUVFuKeu9vKI/edit).
1. **Important:** For a 12-month contract with a March 4, 2019 start date, the end date would be March 3, 2020. Any following contract after this 12-month period would have a start date of March 4, 2020.
1. [ ] People Connect: Following the guidelines documented in the [job aid](https://docs.google.com/document/d/1fKRau2JJcuHLxsHep4Z6zi6ylTSQxeQ-VsIhPKPITFc/edit) update the team members Probation Period details in Workday.
1. [ ] People Connect: If this was not done during the offer signing process and the new team member confirms this in a comment in this issue, send the payroll form and wage tax form to HR Savvy along with the contract of employment, copy of the residence permit (if applicable), copy of the debit card, and passport. All forms should be filed in Workday, reach out to HR Savvy to get the forms and file them.
1. [ ] People Connect: If the position is in development or research and involves `writing code to create new software.`, it likely qualifies for [WBSO (R&D tax credit)](https://about.gitlab.com/handbook/people-operations/#wbso-rd-tax-credit-in-the-netherlands); ping `@jgladen` and `@jonathanvenezia` in this issue so that they may review if team member should be added to the [WBSO sheet](https://docs.google.com/spreadsheets/d/1pNprxjsyJA45wGV38zGVyhotKWRJ99iQ8CvRZtz_H8w/edit?ts=5d2c3441#gid=0). The tax team will add team member to WBSO sheet if applicable. 
1. [ ] People Connect: Ensure that the team member has uploaded their [Certificate of Good Conduct or VOG (Verklaring Omtrent het Gedrag)](https://internal-handbook.gitlab.io/handbook/people-group/people-operations/people-connect/frequently_requested/#netherlands-certificate-of-good-conduct) within thirty days of their start date. If the team member does not complete this task within the alloted 30 days, comment in this issue to notify the Sr. Background Check Specialist `@mattdagostino`.

</details>

<details>
<summary>Legal</summary>

1. [ ] @sarahrogers Confirm that team member has acknowledged receipt and review of safety regulations. 

</details>
