#### Customer Success

<details>
<summary>Manager</summary>

1. [ ] Manager: Notify the new member that they should be invited to a Sales Quick Start Classroom which is their Customer Success onboarding experience. 
1. [ ] Manager: Review the Sales Quick Start Handbook sections detailing [Upcoming SQS Workshops](https://about.gitlab.com/handbook/sales/onboarding/#upcoming-sales-quick-start-sqs-workshops) and the [SQS Remote Agenda](https://about.gitlab.com/handbook/sales/onboarding/SQS-workshop/#sales-quick-start-remote-agenda) to be aware of the timing of live onboarding sessions. New team members are automatically enrolled into the next SQS cohort on the first Monday of each week. Generally a live SQS is held every month consisting of team members hired in the previous month. Team members are made aware of SQS and role based onboarding tasks via the onboarding issue, being added to a cohort specific slack channel and via email notification during their first week with GitLab. 
1. [ ] Manager: Invite to sales meeting.
1. [ ] Manager: Invite to weekly standups slack channel by logging into the geekbot app, going to standup configuration and adding a participant. 
1. [ ] Manager: Schedule weekly 1:1 meeting.
1. [ ] Manager: Grant access to the [Sales Folder](https://drive.google.com/drive/folders/0BzQII5CcGHkKSFFJWkx3R1lUdGM?usp=sharing) in our Google Docs.
1. [ ] Manager: Comment on the Role Entitlement issue, stating the correct SalesForce permission group the new team member should be assigned to, or create an [Access Request](https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/access-requests/).
1. [ ] Manager: Log in to [Tilt365](https://www.tilt365.com/) and purchase the $49 True Tilt Personality Profile (without the 2, E-Learn modules) and with the 15% coupon. See [this document](https://docs.google.com/document/d/16ojLVMbOzHrdhm43SkQHGjgY310kEyhLsL6jPQgIZfY/edit) for login information and coupon code. Please use your own credit card and submit for reimbursement.

</details>


<details>
<summary>New Team Member</summary>

1. [ ] New team member: In the [Sales Folder](https://drive.google.com/drive/folders/0BzQII5CcGHkKSFFJWkx3R1lUdGM?usp=sharing), familiarize yourself with: [Our Sales Agenda](https://docs.google.com/document/d/1YF4mtTAFmH1E7HqZASFTitGQUZubd12wsLDhzloEz3I/edit) and [Competition](https://about.gitlab.com/devops-tools/).
1. [ ] New team member: Familiarise yourself with the GitLab Sales Learning Framework listed in the [sales training handbook](https://about.gitlab.com/handbook/sales-training/).
1. [ ] New team member: Review the [Sales Compensation Plan](https://about.gitlab.com/handbook/finance/sales-comp-plan/).
1. [ ] New team member: Take the Tilt365 exam, upon receiving access (see Manager action item)
1. [ ] New team member: Please login to Level Up - (GitLab’s Learning Experience Platform) and begin working through your role based onboarding Journey “[Solutions Architect: 30-60-90 Onboarding Journey](https://levelup.gitlab.com/access/saml/login/internal-team-members?returnTo=https://levelup.gitlab.com/learn/learning-path/solutions-architect-30-60-90-onboarding-journey)”. This Learning Path is designed to accelerate your time to productivity by focusing on the key sales-specific elements you need to know and be able to do within the first three months at GitLab
1. [ ] New team member: If you are in the Commercial SA Organization, please [create a commercial SA onboarding](https://gitlab.com/gitlab-com/customer-success/solutions-architecture/commercial-onboarding/-/issues/new?issuable_template=SA%20Onboarding%20Checklist&issue[title]=SA_NAME%20-%20Commercial%20SA%20Onboarding) issue and proceed with completing it.


</details>

<details>

<summary>Sales Enablement</summary>

1. [ ] Sales Enablement (@jblevins608): Add new sales team member to the current [Sales Quick Start Workshop](https://about.gitlab.com/handbook/sales/onboarding/SQS-workshop/#sales-quick-start-workshop) cohort. You will receive an invitation to a public slack channel exclusively for your onboarding cohort, #sales-quick-start (followed by a number) which will contain more information about the onboarding workshop. To learn more about the onboarding program and what to expect, take a look at the handbook page on [graduating from sales onboarding.](https://about.gitlab.com/handbook/sales/onboarding/graduating-SQS/#graduating-from-sales-onboarding)

</details>



