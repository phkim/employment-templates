## For Support Only

<details>
<summary>New Team Member</summary>

1. [ ] New team member: Introduce yourself to your team!
   1. [ ] Update the [Support Week in Review Form](https://gitlab-com.gitlab.io/support/toolbox/forms_processor/SWIR/) with **Category** `Team member updates`, your **GitLab Username**, you can put 'New team member' in **Short descriptive title**, then in the **Full details** box, write an entry with your name, location, the date you started, a quick blurb about your experience, personal interests and what drew you to GitLab support. No tags are needed.
   1. [ ] Now, also format and send the introduction post you just created to the `#support_team-chat` Slack channel. Welcome to
      [multi-modal communication](https://about.gitlab.com/handbook/communication/#multimodal-communication), key to effective communication at GitLab!
1. [ ] New team member: **After you've [added yourself to the Team Page](#adding-yourself-to-the-team-page)**, submit an MR to
   update your entry in the **Support** Team Page:
   1. To update the Support Team Page, see [this Readme](https://gitlab.com/gitlab-com/support/team/-/blob/master/README.md)
      about working with the `support-team.yaml` file
   1. In your entry in the file:
      1. indicate the SGG to which you've been assigned
      2. check whether your pronouns are listed the way you want
1. [ ] New team member: Familiarize yourself with the [engineering handbook](https://about.gitlab.com/handbook/engineering) and relevant pages linked from there.
1. [ ] New team member: Once you get [ZenDesk](https://gitlab.zendesk.com/agent/dashboard) access, complete the following.
    1. [ ] Add a profile picture to your Zendesk account
    1. [ ] Let your manager know if you were not able to create an account in Zendesk for some reason.
    1. [ ] Under your profile in Zendesk, it should read 'Support Staff'. If it reads Light Agent, inform your manager.
1. [ ] New team member: [Request access to ChatOps.](https://docs.gitlab.com/ee/development/chatops_on_gitlabcom.html#chatops-on-gitlabcom)
1. [ ] New team member: Team Calls. To keep up with the changes and happenings within the Support team, we have team calls every week. Every member of the support team is encouraged to join so they can also state their opinions during the call.
   1. [ ] Read up on the [calls the Support Team uses to sync up](https://about.gitlab.com/handbook/support/#weekly-meetings) and make sure you have the ones that pertain to you on your calendar.
   1. [ ] Identify agendas for those meetings, and read through a few past meetings in the document.
   1. [ ] Verify that you have a 1:1 scheduled with your manager and you have access to the agenda for that meeting.
1. [ ] New team member: Add the Support Time Off calendar as an [additional calendar in PTO by Roots](https://about.gitlab.com/handbook/support/support-time-off.html#one-time-setup-actions) to ensure any time off you schedule automatically gets added to the team calendar as well as your own.
1. [ ] New team member: Learn about [Support's key Slack channels](https://about.gitlab.com/handbook/support/#slack) and join them. If you have questions, discuss with your manager.
1. [ ] New team member: Read about [Support's onboarding learning pathway](https://about.gitlab.com/handbook/support/training/#support-onboarding-pathway) to understand the different modules you'll be assigned based on your role.
1. [ ] New team member: Get familiar with the [Support Team Meta project](https://gitlab.com/gitlab-com/support/support-team-meta), the issue tracker for all discussions around all things support.
   1. [ ] Consider configuring [Custom Notification settings](https://docs.gitlab.com/ee/user/profile/notifications.html#change-level-of-project-notifications) for this project, to be notified on all newly created issues. This way you'll be aware of new discussions when they're started.
   **Note:** You'll still need to enable Notifications for specific issues you want to follow to be notified of further updates.
1. [ ] (Required for Manager+) Take the [TeamOps course](https://levelup.gitlab.com/learn/course/teamops) and get the certification in order to gain a better understanding of GitLab's values and see some practical implementations of them.

##### Technical Git Information

1. [ ] Become familiar with GitLab. Familiarize yourself with the dashboard, the projects, and the issue tracker. Become familiar with the `README.md`s for these projects:
   * [GitLab www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/README.md)
   * [GitLab](https://gitlab.com/gitlab-org/gitlab/blob/master/README.md)
   * [GitLab FOSS Edition](https://gitlab.com/gitlab-org/gitlab-foss/blob/master/README.md).
**Note:** that the Gitlab FOSS (Free Open Source Software) Edition is a read-only mirror of GitLab with the proprietary software removed. [This issue](https://gitlab.com/gitlab-org/gitlab/issues/13304) covers the merging of the Enterprise and Community editions.
1. [ ] Refer to [GitLab Docs](https://docs.gitlab.com/ee/README.html) for information on features of GitLab.
1. [ ] Download and [get started with Git](https://docs.gitlab.com/ce/gitlab-basics/start-using-git.html).  It is important for all team members to understand how to work both on the web interface and locally. The team is happy to assist in teaching you git.
1. [ ] If you would like to make any improvements to this **For Support Only** section, a good exercise in GitLab/git would be to make a merge request to change the [Support onboarding template](https://gitlab.com/gitlab-com/people-group/people-operations/employment-templates/-/blob/main/.gitlab/issue_templates/onboarding_tasks/department_customer_support.md).
1. [ ] Consider subscribing to [status.gitlab.com](https://status.gitlab.com) to stay aware of active incidents.

</details>


<details>
<summary>Manager</summary>

1. [ ] Manager: **Before your new team member's start date**:
   1. [ ] Follow the process described in the [SGG FAQ](https://about.gitlab.com/handbook/support/support-global-groups/sgg-faq.html#8-how-does-sgg-fit-into-the-onboarding-process-for-new-ses) to determine the group to which you will assign your new team member.
   1. [ ] Add a comment to this issue to indicate the group assignment (*e.g.* "You will be in the Maple group").
   1. [ ] Post a message in the group's Slack channel announcing the arrival of a new group member.
      - [ ] Encourage the group to welcome them on their start date.
      - [ ] Ask them to add the new team member as an `Owner` to their GitLab.com SGG group.
   1. [ ] Add an item to the baseline Access Request to have your new team member added to the appropriate SGG Slack **groups**.
      - Not to be confused with the SGG Slack _channels_ that the team member can easily join themselves.
      - This step _requires IT_ to add the new member to both relevant groups: `@spt-sggname` and `@spt-sggname-region`.
1. [ ] Manager: During your introductory meeting with your new Support Team Member, introduce them to the concept of [Support Global Groups](https://about.gitlab.com/handbook/support/support-global-groups/) and remind them of their group assignment.
1. [ ] Manager: Identify other Support team members who are starting at around the same time (1-3 weeks either side of your team member's start date). Region is not important. Encourage your new team member to Coffee Chat with these people, and rely on each other as they go through the onboarding process.
1. [ ] Manager: At the beginning of the new team member's second week, open the [New Support Team Member Start Here](https://gitlab.com/gitlab-com/support/support-training/-/issues/new?issuable_template=New%20Support%20Team%20Member%20Start%20Here) issue and provide the link in a comment below this onboarding checklist.
1. [ ] Manager: If the new team member will be a ZenDesk Admin, make sure to include that in the access request.
1. [ ] Manager: If the new team member is going to be working US Federal tickets (U.S. Citizens only), open a [Individual or Bulk access request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Individual_Bulk_Access_Request) to have them added as an agent in the Zendesk US Federal Instance and assign it to `@jcolyer`.
1. [ ] Manager: The new member will be added to the following calendars with inclusion in `GSuite supportgroup` as part of their standard entitlements. Verify they have the correct access. NOTE: If needed sooner than that issue being provisioned, you can [manually add new team member](https://support.google.com/calendar/answer/37082?hl=en#:~:text=Click%20More-,Settings%20and%20sharing.,Click%20Send.) to calendars.
   1. [ ] GitLab Support
   1. [ ] Support Time Off
   1. [ ] Support OOO - <region>
1. [ ] Manager: Help your new team member to introduce themself
   1. [ ] Talk to them about doing an introduction at the next regional team meeting, and add it to the agenda.
1. [ ] Manager: Create a calendar reminder to go over [The 90-Day Checkin](https://docs.google.com/document/d/1yTy8z0UQv84RmCkt3dLAYR8SWAXfcR7dcCnkixrbhKk/) with the new team member during the week of their 90-day milestone. Instructions are in the document.
1. [ ] Manager: Create calendar reminders to review the [Areas of Focus](https://gitlab-com.gitlab.io/support/team/areas-of-focus.html) percentages for the team member (values are taken from [support-team.yaml](https://gitlab.com/gitlab-com/support/team/-/blob/master/data/support-team.yaml)). Suggested intervals: 90 days, 120 days; goal is to reduce Onboarding percentage to zero. 
</details>

<details>
<summary>Buddy</summary>

1. [ ] Buddy: Review the docs on being a [Support Onboarding Buddy](https://about.gitlab.com/handbook/support/training/onboarding_buddy.html),
   which builds on the general [GitLab onboarding buddy docs](https://about.gitlab.com/handbook/people-group/general-onboarding/onboarding-buddies/).
1. [ ] Buddy: Consider scheduling a recurring, weekly pairing session to support your buddy with fully transitioning into the role.
1. [ ] Buddy: When your buddy finishes their onboarding issue, check that they have been added to:
   - [ ] both their SGG-specific Slack groups (`@spt-sggname` and `@spt-sggname-region`)
     - To check this, select "People & user groups" in the Slack sidebar, then switch to the "User groups" tab, search for the `sggname` and check for their membership in both `@spt-sggname` and the correct `@spt-sggname-region`.
     - In case they are not in both groups: Please tag both the team member and their manager in a comment on this issue. Ask them to either modify the baseline Access Request according to the "Manager" steps of this issue, or to create a new AR to be added to both groups.
   - [ ] their SGG-specific GitLab.com group
     - To check this, browse to `https://gitlab.com/groups/gitlab-com/support/sgg-sggname/-/group_members?with_inherited_permissions=exclude` (replace `sggname`) and verify that they are a "Direct member" with `Owner` level permissions.
     - In case they are not: Add them as `Owner` yourself (if they're in the same SGG as you are), or post a message in their SGG's Slack channel and ask someone in that SGG to do so. Then, tag both the team member and their manager in a comment on this issue, informing them about your action and reminding them about the corresponding "Manager" steps of this issue.
</details>
