#### Sales Division

<details>
<summary>Sales Enablement</summary>

1. [ ] Sales Enablement (@jblevins608): Add new Sales Team Member to Sales Quick Start Learning Path in EdCast. New sales team member will receive an email prompting them to login to EdCast to begin working through the Sales Quick Start Learning Path. This Learning Path is designed to accelerate the new sales team member's time to productivity by focusing on the key sales-specific elements they need to know and be able to do within their first several weeks at GitLab.

</details>

<details>
<summary>Manager</summary>

1. [ ] Manager: Grant access to the [Sales Folder](https://drive.google.com/drive/folders/0BzQII5CcGHkKSFFJWkx3R1lUdGM?usp=sharing) in our Google Docs.
1. [ ] Manager: Inform Sales Operations (@tav_scott @james_harrison) what territory the new team member will be working and if they have a paired SDR.
1. [ ] Manager: Create and submit an issue using [this issue template](https://gitlab.com/gitlab-com/sales-team/field-operations/sales-operations/issues/new#) to inform Sales Ops of the territory that the new team member will assume, paired SDR (if any), and identified opportunity holdovers that should remain with the current owner (if any).
Accounts will be reassigned to new hire and transition plan needs to be approved per account.
1. [ ] Manager: Slack Sales Analytics (@Swetha) the new team member's quota information for upload into Salesforce.

</details>

<details>
<summary>Sales Strategy</summary>

1. [ ] Sales Strategy (@Swetha): Set BCR, SCR (if applicable), and prorated quota and deliver participant plan/schedule
1. [ ] Sales Strategy (@Swetha): Deliver Participant Schedule

</details>

<details>
<summary>New Team Member</summary>

1. [ ] New team member: Complete your Sales Quick Start Learning Path (see your email for an invitation from Google Classroom).
If you have questions, please reach out to Tanuja (@tparuchuri).
1. [ ] New team member: For all roles EXCEPT PubSec Inside Sales Rep and SMB Customer Advocate, please consult with your manager to determine whether or not you need access to Zendesk Light Agent.
If yes, follow [these instructions](https://about.gitlab.com/handbook/support/internal-support/#light-agent-zendesk-accounts-available-for-all-gitlab-staff) to request access.
Note: SMB Customer Advocates are provisioned "GitLab Staff Role" access to Zendesk and PubSec Inside Sales Reps are provisioned "Light Agent" access to Zendesk-federal within their role-based access request template above.
PubSec Strategic Account Leaders may rely on their PubSec Inside Sales Rep for Zendesk-federal related matters.

</details>

<details>
<summary>Sales Ops</summary>

1. [ ] Marketing (@tav_scott): Two weeks after start date, go live with update to Territory Model in LeanData with new territory assignments.

</details>

<details>
<summary>Global Alliances</summary>
Review the following Alliance-specific documents, QBR decks and Business Plans:
- [ ]  Alliance Org chart [May 2022 Version](https://docs.google.com/presentation/d/1tGh_GKa28ViQOO53it9sfSHE_EipcbewsdS4sYW9n3o/edit#slide=id.g11c6be51354_0_0)
- [ ]  FY23 Sales Kick Off.  [EdCast recording](https://gitlab.edcast.com/channel/sales-kickoff-sko-2022) and [Highspot link](https://gitlab.highspot.com/spots/615dd82071cff4c4b2bcbc32?list=615de3de145718c4b165e083&overview=true)
- [ ]  [SKO2022 - Alliance Keynote Presentation](https://docs.google.com/presentation/d/1ccvdJ7V8XNw11dPTw7Ilrp0kFw2BtCmo90zo62MiDyI/edit#slide=id.g1084bc36e46_0_0)
- [ ]  [Partner SKO2022 - Alliance Presentation](https://docs.google.com/presentation/d/1zGhqRdpW0qMm6vrushGe1jo9fUshzVYcIkui5LMB1RI/edit#slide=id.g1084bc36e46_0_0)
- [ ]  Review past Alliance QBRs [QBR FY23 Q1 Performance](https://docs.google.com/presentation/d/1RoCWQRCwqSqrU70qJQue0D2dedM5sDFdu8Q5XvtpK78/edit#slide=id.g112ab5f9708_0_0) 
- [ ]  Review past Alliance QBRs [QBR FY22 Q4 Performance](https://docs.google.com/presentation/d/1LmgN6h4TNxElgwM0ia9IawGs2DvcJLKZ-dZZO5Y_IqM/edit#slide=id.g112ab5f9708_0_0) 
- [ ]  Review past Alliance QBRs [QBR FY22 Q3 Performance](https://docs.google.com/presentation/d/1oSCkONOgS-Ku0YuBX0LyPGNk6xmwfl6yf1tB1qdMU18/edit#slide=id.gd999adb0ca_1_161) 
- [ ]  Review past Alliance QBRs [QBR FY22 Q2 Performance](https://docs.google.com/presentation/d/1PSHUtCJZqvtiWw1vK2E2KTKukjPsYaNsX4O4zbvfROo/edit#slide=id.gd999adb0ca_1_161) 
- [ ]  Business Plans [AWS FY23](https://docs.google.com/document/d/1FfxmFpZqUglJoxBPr60BhavFPOd7_Ap1dByOxJl-YQk/edit#heading=h.gjdgxs)
- [ ]  Business Plans [AWS](https://docs.google.com/document/d/1RrU-cyEAtPU2xeyTS2wOggD7K2GAx2ezVBBPH8ySmm8/edit#heading=h.gjdgxs)
- [ ]  Business Plans [Google Cloud](https://docs.google.com/document/d/1apO__C6FjAmuapJBbJsfbcpN8MonEdq9kBEuK0vmzD8/edit#heading=h.gjdgxs)
- [ ]  Business Plans [IBM -old](https://docs.google.com/document/d/1O1XkUGYMKfR25EvVhjf0dNyJJ9nmW34Bw9yMKr3foL8/edit#heading=h.urfa5rq4ihpk)
- [ ]  Business Plans [IBM Cloud Pak](https://docs.google.com/document/d/14b2Fei38aWWl_JwWOt97vo7ugOu7H6e_MtTZPWoNWQY/edit)
- [ ]  Business Plans [Cisco](https://docs.google.com/document/d/1rDkJQJYuvd7B4a_lhsYtHV2BIP5KKyxnqTFLJ08uyi8/edit)
- [ ]  Business Plans [HPE](https://docs.google.com/document/d/1FJZteZnk5UYbKHlu-zBz-TJv7zEN2VWou7042YMJcQw/edit#)
- [ ]  Business Plans [Oracle](https://docs.google.com/document/d/1kqcNxcnGr6zTf7VPGNB5A1Vl8_oNlr7sIpgPb3yqvp8/edit#heading=h.gjdgxs)
- [ ]  [Alliances-Group-Conversation-2022-03-31](https://docs.google.com/presentation/d/1_lCIZcIamu3Gm9q_tX14uFv1EO35QpdNhBAWV6VZTao/edit#slide=id.gf6b0ade7f9_0_922)
- [ ]  Search for Alliance Update on 2022-03-31 [CY2022 - Group Conversation Agenda](https://docs.google.com/document/d/1ngcT8QDA-_iFkUe8bWLJ1jpEq9cv9sWMwEFdbtWLY2o/edit)
Review the following Channel and Alliance Edcasts, Handbook Pages and Processes:
- [ ]  [Alliances: Working With and Co-Selling With AWS](https://gitlab.edcast.com/insights/ECL-848bf136-9c27-4c03-99cf-0fdc35184e40)
- [ ]  [Alliances: Working With and Co-Selling With IBM](https://gitlab.edcast.com/insights/ECL-47a1907b-dace-4d81-b8a6-3761b4b98820)
- [ ]  [Alliances: Working With and Co-Selling With Google](https://gitlab.edcast.com/insights/ECL-3b7cd71f-a309-461b-ad40-c147f76d5a3e)
- [ ]  [Building Pipeline with your Partners - MDF and Marketing Campaigns](https://gitlab.edcast.com/insights/ECL-97e5297d-4067-45c4-a9d4-fac241775d85)
- [ ]  [Planning for Joint GitLab and Partner Success](https://gitlab.edcast.com/insights/ECL-fad02cd0-772a-4d6e-9cd8-37b9e95af7e6)
- [ ]  [Growing Our Partner Business](https://gitlab.edcast.com/insights/ECL-9f2e7565-0f2d-4328-ba7a-ebb76c137615)
- [ ]  [GitLab Deal Registration and Rules of Engagement Training](https://youtu.be/R5BSo92T8sw)
- [ ]  Review [GitLab Alliances Handbook Page](https://about.gitlab.com/handbook/alliances/) 
- [ ]  Review [GitLab Alliances INTERNAL ONLY Handbook Page](https://gitlab-com.gitlab.io/alliances/alliances-internal/) 
SalesForce Access
- [ ] https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Individual_Bulk_Access_Request
- [ ] Follow this process in the [handbook](https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/access-requests/#how-do-i-choose-which-template-to-use)
Add yourself to the following slack channels:
- [ ]  a_google_internal
- [ ]  a_alliance_internal
- [ ]  a_aws_deal_registration
- [ ]  a_gcp_deal_registration
- [ ]  abdms
- [ ]  alliance_directors_only
- [ ]  alliance_gcp
- [ ]  alliance_gcp_internal
- [ ]  alliances
- [ ]  alliance_team_only_internal
- [ ]  alliances_partnermktg
- [ ]  alliances_program_spend
- [ ]  alliances_sales_ops
- [ ]  alliances-recruiting
- [ ]  #cro
- [ ]  #ceo
- [ ]  #sales
- [ ]  #sales-vp
- [ ]  #competition
- [ ]  #field-fyi
- [ ]  #new_team_members
- [ ]  #marketing
- [ ]  #marketing-insights
- [ ]  #stock-admin
- [ ]  #thanks
- [ ]  #team-member-updates
Schedule 1:1 Coffee Chats:
1. [ ] All current Alliance team members in Alliance. See [Org Chart](https://comp-calculator.gitlab.net/org_chart)
1. [ ] CRO Leadership. CRO. Michael McBride
1. [ ] CRO Leadership. VP Enterprise. Mike Pyle
1. [ ] CRO Leadership. VP Commercial. Ryan O'Nell
1. [ ] CRO Leadership. VP Customer Success. David Sakamoto
1. [ ] CRO Leadership. VP Sales Ops. David Hong
1. [ ] CRO Leadership. VP Channel. Michell Hodges
1. [ ] CRO Leadership. VP Alliances. The Honorable Sir Nima Badiey
1. [ ] Marketing. Sales Development. LB Larramendy. 
1. [ ] Marketing. Corp Marketing. Melissa Smolensky
1. [ ] Marketing. Product Marketing. Dave Steer
1. [ ] Marketing. Partner Marketing. Coleen Greco
1. [ ] People/HR. Alliance Recruiting. Jake Foster
1. [ ] Legal. Contracts and Legal Ops. Robert Nalen
1. [ ] Channel Ops. Collen Farris
1. [ ] Channel. Programs. Ed Cepulis
1. [ ] Channel. Federal. Chris Novello
1. [ ] Channel. EMEA. Matthew Coughlan
1. [ ] Channel. Amer. Annie Rooke
1. [ ] Channel. APAC. Dirk de Vos
1. [ ] Alliance. Google Alliance Director. Jason Mero
1. [ ] Alliance. Amazon Alliance Director. Pete Goldberg
1. [ ] Alliance. IBM Alliance Director. Brett Egloff
1. [ ] Alliance. Platform Alliance Director. Vick Kelkar
1. [ ] Alliance. Tech/ISV Alliance Manager. Jaimie Kander
1. [ ] Alliance. All global ABDMs
:smile:
</details>
--------------------------------------------
<details>
<summary>Global Channels</summary>
* Job Specific Tasks
  * Add yourself to the following Slack Channels:
- [ ] channel-fyi
- [ ] channel-programs-ops
- [ ] channel-services
- [ ] channel-sales
- [ ] channels-emea
- [ ] channels-amer
- [ ] channel-japan-sales
* Complete the following Channel Specific Onboarding Enablement:
- [ ] [Working With and Co-Selling With Partners](https://gitlab.edcast.com/insights/ECL-9a30df21-dff0-4ac5-8ffa-e67429860dfd)
- [ ] [Alliances: Working With and Co-Selling With AWS](https://gitlab.edcast.com/insights/ECL-848bf136-9c27-4c03-99cf-0fdc35184e40)
- [ ] [Alliances: Working With and Co-Selling With IBM](https://gitlab.edcast.com/insights/ECL-47a1907b-dace-4d81-b8a6-3761b4b98820)
- [ ] [Alliances: Working With and Co-Selling With Google](https://gitlab.edcast.com/insights/ECL-3b7cd71f-a309-461b-ad40-c147f76d5a3e)
- [ ] [Building Pipeline with your Partners - MDF and Marketing Campaigns](https://gitlab.edcast.com/insights/ECL-97e5297d-4067-45c4-a9d4-fac241775d85)
- [ ] [Planning for Joint GitLab and Partner Success](https://gitlab.edcast.com/insights/ECL-fad02cd0-772a-4d6e-9cd8-37b9e95af7e6)
- [ ] [Growing Our Partner Business](https://gitlab.edcast.com/insights/ECL-9f2e7565-0f2d-4328-ba7a-ebb76c137615)
- [ ] [GitLab Deal Registration and Rules of Engagement Training](https://youtu.be/R5BSo92T8sw)
- [ ] [REVIEW: FY22 GitLab Channel Program Updates](https://www.youtube.com/watch?v=6Ngt3Pit9S4)
- [ ] [REVIEW: FY22 GitLab Channel Tagging and Reporting](https://www.youtube.com/watch?v=JSzWVQjIChM)
- [ ]  Request Admin Access to Partner Portal (ImPartner) via emailing partnerhelpdesk@gitlab.com 
- [ ]  Review [Channel Partner Program Handbook Page](https://about.gitlab.com/handbook/resellers/)
- [ ]  Review [Channel Sales Handbook Page ](https://about.gitlab.com/handbook/sales/channel/)
- [ ]  Review [Channel Operations Handbook Page](https://about.gitlab.com/handbook/sales/field-operations/channel-operations/)
- [ ]  Review [Channel Services Handbook Page](https://about.gitlab.com/handbook/resellers/services/) 
- [ ]  Review [Channel Training, Certification and Enablement Handbook Page](https://about.gitlab.com/handbook/resellers/training/) 
- [ ]  Review [GitLab Alliances Handbook Page](https://about.gitlab.com/handbook/alliances/) 
- [ ]  Review [GitLab Alliances INTERNAL ONLY Handbook Page](https://gitlab-com.gitlab.io/alliances/alliances-internal/) 
* Weeks 2 and 3 of general onboarding, schedule Coffee Chats with:
- [ ] VP WW Channels
- [ ] Senior Director Partner Enablement and Channel Programs
- [ ] Director Channel Operations
- [ ] Regional Sales Director
- [ ] Head of Channel Sales (APAC)
</details> 
