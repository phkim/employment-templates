## For People Connect Only

<details>
<summary>Manager</summary>

1. [ ] Manager: Add team member to the following private Slack channels (as applicable):
   - `#people-group-confidential`
   - `#peopleconnect_ces`
   - `#connect-ops-team`
   - `#peopleconnect_totalrewards`
   - `#payroll-peopleconnect`
   - `#pbp-peopleconnect`
   - `#international-exp`
   - `#employment-surveys`
   - `#offboardings`
   - `#people-ops-tech`
1. [ ] Manager: Add team member to the following public Slack channels (as applicable):
   - `#peopleops-alerts`
   - `#people-group`
   - `#people-connect`
1. [ ] Manager: Add team member to the following Slack groups (as applicable):
   - `people-connect-team`

</details>

<details>
<summary>People Connect</summary>

1. [ ] People Connect: Share the People Connect Shared [Drive](https://drive.google.com/drive/folders/0ABRi3YnroHzzUk9PVA)
1. [ ] People Connect: Invite the team member to the [People Group Calendar](https://calendar.google.com/calendar/embed?src=c_i9nueu50c6ddsq7s6odraihhi0%40group.calendar.google.com&ctz=America%2FNew_York) in Google Calendar.

</details>

