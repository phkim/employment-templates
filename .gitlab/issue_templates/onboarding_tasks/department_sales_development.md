#### Sales Development


<details>
<summary>Manager</summary>

1. [ ] Manager: Create and submit an issue using [this issue template](https://gitlab.com/gitlab-com/marketing/marketing-operations/-/issues/new?issuable_template=leandata_change_sdralignment) to inform Marketing Operations of the territory that the new team member will assume and when lead routing should be started.
1. [ ] [BDR Managers Only] Copy [this](https://docs.google.com/document/u/1/d/1ymdIfeGhFzFLJkz0_1qZhzVlLMaTpNaiTowfdbVJEao/copy) document, update it and share it with your new team member.
1. [ ] [US Public Sector Only] Give new team member access to resources contained in [Level Up](https://levelup.gitlab.com/access/saml/login/internal-team-members?returnTo=https://levelup.gitlab.com/learn/learning-path/us-public-sector).
1. [ ] Manager: Review the Sales Quick Start Handbook sections detailing [Upcoming SQS Workshops](https://about.gitlab.com/handbook/sales/onboarding/#upcoming-sales-quick-start-sqs-workshops) and the [SQS Remote Agenda](https://about.gitlab.com/handbook/sales/onboarding/SQS-workshop/#sales-quick-start-remote-agenda) to be aware of the timing of live onboarding sessions. New team members are automatically enrolled into the next SQS cohort on the first Monday of each week. Generally a live SQS is held every month consisting of team members hired in the previous month. Team members are made aware of SQS and role based onboarding tasks via the onboarding issue, being added to a cohort specific slack channel and via email notification during their first week with GitLab.

</details>


<details>
<summary>New Team Member</summary>

1. [ ] New SDRs: Copy [this document](https://docs.google.com/spreadsheets/d/1qbfm2XN8KJWaqU2r8g_zWGCMr-26JYAP4K_pk7VGrSE/copy) and share it with your manager and onboarding buddy. This is an extended checklist for sales development to help you stay focused on learning items relevant for your new role.
1. [ ] New BDRs: Copy [this document](https://docs.google.com/spreadsheets/d/1gc7a2E-ziRfjYukSzozDC5M0f2yq-CgBv5GMQgTYHJM/copy) and share it with your manager and onboarding buddy. This is an extended checklist for business development to help you stay focused on learning items relevant for your new role.
1. [ ] New associate BDRs: Copy [this document](https://docs.google.com/spreadsheets/d/1fqU7NAfUr27UAVzzMG7COxFxww2K3uwQeH7zAn9WGbI/copy) and share it with your manager and onboarding buddy. This is an extended checklist for associate business development to help you stay focused on learning items relevant for your new role.
1. [ ] New SDR team member: Download Google Chrome and set it as your default browser. Google Chrome is the only browser compatible with Outreach.
1. [ ] New team member: Please [add your @gitlab.com email address as an alternative](https://www.linkedin.com/help/linkedin/answer/60/adding-or-changing-your-email-address-for-your-linkedin-account?lang=en) on your LinkedIn profile. This will allow you to easily access LinkedIn Sales Navigator once you have received the email invite.
1. [ ] New team member: Connect ZoomInfo to Salesforce: Once you receive access to ZoomInfo, log in > click your name in the top right hand corner > click 'Account Settings' > at the top left click 'CRM Settings' > ensure current CRM = Salesforce and Connect to = production instance > click authorize.
1. [ ] New team member: Skip through our [SDR Handbook page](https://about.gitlab.com/handbook/marketing/revenue-marketing/sdr/).

</details>


<details>
<summary>Sales Enablement</summary>

1. [ ] Sales Enablement (@jblevins608): Add new sales development team member to the next live SQS cohort.

</details>


<details>
<summary>Sales Development Operations</summary>

1. [ ] Sales Development Operations (@ebao2): Update sdr/bdr/sdrbdr email aliases for new team member.

</details>
