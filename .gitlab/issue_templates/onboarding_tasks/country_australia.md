### Day 1 - For Team Members in Australia only

<details>
<summary>New Team Member</summary>

1. [ ] New team member: Please provide the following to nonuspayroll@gitlab.com:
   * [ ] Completed [Tax File Number Declaration](https://www.ato.gov.au/uploadedFiles/Content/IND/Downloads/TFN_declaration_form_N3092.pdf).
   * [ ] Information about your Superannuation account. Provide your member number as well as the ABN or USI of the Super fund. This can be found on your fund's website.
1. [ ] New team member: You will be prompted to populate your banking information in Workday via the [Add and Edit Payment Elections Task](https://drive.google.com/file/d/19vH0uKMlDQDF8EaGEg2dW6sNIcnjJSrM/view?usp=sharing).

</details>

<details>
<summary>People Connect</summary>

1. [ ] People Connect: Following the guidelines documented in the [job aid](https://docs.google.com/document/d/1fKRau2JJcuHLxsHep4Z6zi6ylTSQxeQ-VsIhPKPITFc/edit) update the team members Probation Period details in Workday.
1. [ ] People Connect: Verify that the new team member's Legal Name on their Photo ID matches the Legal Name entered into Workday.

</details>
