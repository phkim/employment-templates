## For People Managers

1. [ ] People Connect: Confirm the direct reports in Workday were re-assigned automatically
1. [ ] @gitlab-com/people-group/peopleops-eng: Check if they had nominations they had not approved yet for their direct and indirect (second level) reports.
1. [ ] Update BambooHR direct reports to offboarded team members manager (this is for PTO by Roots sync).
