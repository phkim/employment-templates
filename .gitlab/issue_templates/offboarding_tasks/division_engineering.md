## FOR ENGINEERING ONLY (Devs, PEs, SEs, UX)

<summary>Manager</summary>

1. [ ] Manager: Remove former GitLab Team Member's' GitHub.com account from the [gitlabhq organization](https://github.com/orgs/gitlabhq/people) (if applicable).
1. [ ] For former Developers (those who had access to part of the infrastructure), and Production GitLab Team Members: copy offboarding process from [infrastructure](https://dev.gitlab.org/cookbooks/chef-repo/blob/master/doc/offboarding.md) for offboarding action.
1. [ ] Manager: Remove access to PagerDuty if applicable.
1. [ ] Manager (For Build Engineers): Remove team member as a member to the GitLab Dev Digital Ocean account https://cloud.digitalocean.com/settings/team.
1. [ ] Manager: Remove from GitLab Docker Hub teams if applicable.
1. [ ] Manager: Replace mentions in handbook's [stages.yml](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/stages.yml) as this is the look-up reference for many processes. 
1. [ ] Manager: Remove former team member from the Async Team Retrospective [team.yml](https://gitlab.com/gitlab-org/async-retrospectives/-/blob/master/teams.yml) file, add yourself or temporary backup instead.
1. [ ] Manager: Remove former team member from team Slack bots such as daily stand-up, if applicable.
1. [ ] Manager (For Ops Section): Remove former team member from the [ops-section-retro.md](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/.gitlab/issue_templates/ops-section-retro.md) issue template.
1. [ ] Manager (For R&D team): Remove former team member from the [product-development-retro.md](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/.gitlab/issue_templates/product-development-retro.md) issue template.
1. [ ] Manager: Remove former team member from the [_categories-names.erb](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/sites/handbook/source/includes/product/_categories-names.erb) file.
1. [ ] Manager: (For Anti-abuse team members) Remove from [Arkose Labs](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/tech_stack.yml) team if applicable. 

<summary>IT Ops</summary>

1. [ ] IT Ops @gitlab-com/business-technology/team-member-enablement : Remove any development VMs. Send a merge request to [the dev-resources repo](https://gitlab.com/gitlab-com/dev-resources) to remove `dev-resources/name-surname.tf`. Follow the instructions [here](https://gitlab.com/gitlab-com/dev-resources/tree/master/dev-resources#how-do-i-delete-an-instance-i-dont-need-anymore).

<summary>Product Licenses</summary>

- [ ] JetBrains (@leipert, @samdbeckham): Go to the [user management](https://account.jetbrains.com/login) and search for the team member, revoke their licenses.


## Infrastructure

#### <summary>Mohammed Al Kobaisy @malkobaisy</summary>

<table>
  <tbody>
		<tr>
			<td> AWS (Production)  </td>
			<td>

- [ ] Access Deprovisioned - AWS (Production)
- [ ] Not Applicable to Team Member - AWS (Production)

</td>
		</tr>
		<tr>
			<td> AWS (Support / Staging)  </td>
			<td>

- [ ] Access Deprovisioned - AWS (Support / Staging)
- [ ] Not Applicable to Team Member - AWS (Support / Staging)

</td>
		</tr>
		<tr>
			<td> AWS (Security)  </td>
			<td>

- [ ] Access Deprovisioned - AWS (Security)
- [ ] Not Applicable to Team Member - AWS (Security)

</td>
		</tr>
		<tr>
			<td> Azure  </td>
			<td>

- [ ] Access Deprovisioned - Azure
- [ ] Not Applicable to Team Member - Azure

</td>
		</tr>
		<tr>
			<td> Chef  </td>
			<td>

- [ ] Access Deprovisioned - Chef
- [ ] Not Applicable to Team Member - Chef

</td>
		</tr>
		<tr>
			<td> Digital Ocean (GitLab Dev)  </td>
			<td>

- [ ] Access Deprovisioned - Digital Ocean (GitLab Dev)
- [ ] Not Applicable to Team Member - Digital Ocean (GitLab Dev)

</td>
		</tr>
		<tr>
			<td> Elastic Cloud  </td>
			<td>

- [ ] Access Deprovisioned - Elastic Cloud
- [ ] Not Applicable to Team Member - Elastic Cloud

</td>
		</tr>
		<tr>
			<td> Fastly CDN  </td>
			<td>

- [ ] Access Deprovisioned - Fastly CDN
- [ ] Not Applicable to Team Member - Fastly CDN

</td>
		</tr>
		<tr>
			<td> GitLab.com Prod/staging rails and db console (ssh)  </td>
			<td>

- [ ] Access Deprovisioned - Gitlab.com (Prod/staging rails)
- [ ] Not Applicable to Team Member - Gitlab.com (Prod/staging rails)

</td>
		</tr>
		<tr>
			<td> customers. -gitlab.com (ssh)  </td>
			<td>

- [ ] Access Deprovisioned - customers. -gitlab.com (ssh)
- [ ] Not Applicable to Team Member - customers. -gitlab.com (ssh)

</td>
		</tr>
		<tr>
			<td> ops.gitlab.net  </td>
			<td>

- [ ] Access Deprovisioned - ops.gitlab.net
- [ ] Not Applicable to Team Member - ops.gitlab.net

</td>
		</tr>
        <tr>
			<td> Does the Team Member own any [ops.gitlab.com pipeline schedules](https://gitlab.com/-/snippets/2121495)? </td>
			<td>

- [ ] Ownership changed from Team Member to bot - ops.gitlab.com pipeline schedules
- [ ] Not Applicable to Team Member - ops.gitlab.com pipeline schedules

</td>
		</tr>
		<tr>
			<td> PackageCloud  </td>
			<td>

- [ ] Access Deprovisioned - PackageCloud
- [ ] Not Applicable to Team Member - PackageCloud

</td>
		</tr>
		<tr>
			<td> Rackspace  </td>
			<td>

- [ ] Access Deprovisioned - Rackspace
- [ ] Not Applicable to Team Member - Rackspace

</td>
		</tr>
</tbody>
</table>

#### @dawsmith @dcurlewis @kwanyangu @afappiano @amoter

<table>
  <tbody>
		</tr>
		<tr>
			<td> CloudFlare  </td>
			<td>

- [ ] Access Deprovisioned - CloudFlare
- [ ] Not Applicable to Team Member - CloudFlare

</td>
		</tr>
</tbody>
</table>

- [ ] @adamhuss: Remove the team member from Nira

