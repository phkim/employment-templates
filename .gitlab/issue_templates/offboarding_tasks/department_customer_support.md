## FOR SUPPORT ENGINEERING/AGENTS ONLY

<summary>Manager</summary>

1. [ ] Zendesk [(general information about removing agents)](https://support.zendesk.com/hc/en-us/articles/203661156-Best-practices-for-removing-agents#2):
   1. [ ] Manager: Remove any triggers related to the agent - https://gitlab.zendesk.com/agent/admin/triggers.
   1. [ ] Manager: Downgrade the agent role to "end-user" - [more information](https://support.zendesk.com/hc/en-us/articles/203661156-Best-practices-for-removing-agents#2).
        + **Warning: This will unassign all tickets from the agent** Consider reducing the "full agent" count on our Zendesk license.
   1. [ ] Manager: Schedule a date to suspend the agent account. [More information](https://support.zendesk.com/hc/en-us/articles/203661156-Best-practices-for-removing-agents#3).
   1. [ ] Manager: Remove team member from [GitHub Support org](https://github.com/GitLab-Support), typically if they are also a ZenDesk admin.
1. [ ] Community Forum:
   1. [ ] Manager: Remove team member from "moderators" group on the [GitLab community forum](https://forum.gitlab.com/).

<summary>Systems</summary>

1. [ ] @leipert, @samdbeckham: Revoke JetBrains licenses. Go to the [user management](https://account.jetbrains.com/login) and search for the team member, revoke their licenses.
1. [ ] @jcolyer: A reminder to check whether the team member was a US citizen and had access to US Federal Zendesk (if applicable, remove the team member)
1. [ ] @jcolyer: Deprovision Access in Status.io


<summary>IT Ops</summary>

1. [ ] IT Ops @gitlab-com/business-technology/team-member-enablement : Remove any development VMs and other resources:
  - [ ] Notifiy one of the deprovisioners listed for [AWS](https://console.aws.amazon.com/) in our [Tech Stack](https://docs.google.com/spreadsheets/d/1mTNZHsK3TWzQdeFqkITKA0pHADjuurv37XMuHv12hDU/edit#gid=1906611965)
  - [ ] Send a merge request to [the dev-resources repo](https://gitlab.com/gitlab-com/dev-resources) to remove `dev-resources/name-surname.tf`. Follow the instructions [here](https://gitlab.com/gitlab-com/dev-resources/tree/master/dev-resources#how-do-i-delete-an-instance-i-dont-need-anymore).
  - [ ] Delete all of the team member's branches from [support-resources](https://gitlab.com/gitlab-com/support/support-resources/-/branches) to delete their GCP instances
1. [ ] IT Ops @gitlab-com/business-technology/team-member-enablement : Revoke GitLab.com admin access if they had it. If you're not sure (i.e. their access predates access requests) ping the manager.
