<summary>Finance</summary>

1. [ ] @kechen408, @rhark: Remove team member from Bank of America, Comerica, Rabobank, HSBC or any other bank accounts (as user or viewer).
1. [ ] @yelena.sokolov: Remove team member from Workiva
1. [ ] @edelongpre: Remove team member from FloQast
1. [ ] Igor Groenewegen-Mackintosh @igroenewegenmackintosh: Remove team member from VATit SaaS
1. [ ] Igor Groenewegen-Mackintosh @igroenewegenmackintosh: Remove team member from Avalara
1. [ ] @Swethakashyap, @lisapuzar, @racheldavies: Remove access from Xactly Incent

<summary>Manager</summary>

1. [ ] Manager: Remove from sales meeting.
