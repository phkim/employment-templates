## FOR SECURITY ONLY

- [ ] @ankelly @dcouture: Remove the team member from HackerOne
- [ ] @ankelly @dcouture: Remove the team member from hackerone-customer.slack.com slack workspace
- [ ] @smanzuik, @jhebden, @skahn007: Remove the team member from Tenable.IO
- [ ] @laurence.bierner: Remove the team member from Rackspace (Security Enclave)
- [ ] @jfuentes2: Remove the team member from Panther SIEM
- [ ] @vmairet: Remove the team member from Tines
- [ ] @dfelton: Remove the team member from BitSight
- [ ] @dfelton: Remove the team member from Anecdotes
 


