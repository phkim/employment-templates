## FOR BUSINESS TECHNOLOGY MEMBERS ONLY:

- [ ] @jeffersonmartin @Karuna16: Remove access to Workato
- [ ] @jeffersonmartin: Remove access to Postman
- [ ] @Karuna16: Remove access to Platypus
- [ ] @dfelton, @Julia.Lake: Remove the team member from ZenGRC
