## FOR MARKETING ONLY

<summary>Sales/Marketing Ops</summary>

1. [ ] Sales/Marketing Ops: Disable User and Other Actions

      * [ ]   Salesforce - @kbetances: REASSIGN RECORDS in Salesforce: Reassign all accounts and open opportunities (do Not Reassign CLOSED WON/LOST OPPS!).
      * [ ]   Salesforce - @emathis: REASSIGN RECORDS in Salesforce: Reassign all leads and contacts.
      * [ ]   Manager DRI: If applicable, please replace the BDR/SDR on any Stage 0 opportunities with exiting SDR/BDR with the new SDR/BDR.

      * [ ]   Outreach - @gillmurphy: First, lock the user, Second, turn off both "Enable sending messages from this mailbox" and "Enable syncing messages from this mailbox".
      * [ ]   ZoomInfo - @RobRosu: Deactivate and remove former team member from ZoomInfo.
      * [ ]   Marketo - @jburton: Remove user if applicable
      * [ ]   ReachDesk - @jburton: Remove team member from ReachDesk if applicable.
      * [ ]   LinkedIn Sales Navigator - @emathis: Disable user, remove from team license.
      * [ ]   Drift - @emathis: IF User: Disable access; IF Admin: Reset password.
      * [ ]   LeanData - @emathis: Remove from any lead routing rules and round robin groups.
      * [ ]   1Password - @christinelee: Rotate any shared login credentials (SFDC API user, Google Analytics, Adwords, Disqus, Facebook, LinkedIn, Zendesk).
      * [ ]   Ringlead - @RobRosu: Remove team member from Ringlead if applicable.
      * [ ]   Demandbase - @emathis @gillmurphy: Remove team member from Demandbase if applicable.
      * [ ]   ARInsights' Architect - Ryan Ragozzine @rragozzine: Remove team member from ARInsights' Architect if applicable.
      * [ ]   Terminus @gillmurphy: Remove team member from Sigstr if applicable
      * [ ]   PathFactory @nikkiroth: Remove team member from PathFactory if applicable
      * [ ]   ON24 @nikkiroth: Remove team from ON24 if applicable
      * [ ]   LogRocket @jburton: Remove team member from LogRocket if applicable
      * [ ]   Litmus @amy.waller: Remove team member from Litmus if applicable
      * [ ]   MailJet @amy.waller: Remove team member from MailJet if applicable
      * [ ]   MailChimp @kjaeger: Remove team member from MailChimp if applicable
      * [ ]   Rev @gillmurphy: Remove team member from Rev if applicable
      * [ ]   Facebook Ad Platform @mnguyen4: Remove team member from Facebook Ad Platform.
      * [ ]   Google Search Console @jburton: Remove the team member from Google Search Console
      * [ ]   DV 360 @mnguyen4: Remove team member from DV 360 if applicable
      * [ ]   Frame.io @Brock_Sumner : Remove team member from Frame.io if applicable
      * [ ]   Figma @jburton: Remove team member from Figma if applicable
      * [ ]   Keyhole @gillmurphy: Remove team member from Keyhole if applicable
      * [ ]   iconik @Brock_Sumner : Remove team member from iconik if applicable
      * [ ]   Vimeo @Brock_Sumner: Remove team member from Vimeo if applicable
      * [ ]   OneTrust @jburton: Remove team member from OneTrust if applicable
      * [ ]   Sitebulb @jburton: Remove team member from Sitebulb if applicable
      * [ ]   Swiftype @jburton: Remove team member from Swiftype if applicable
      * [ ]   LaunchDarkly @jburton: Remove team member from LaunchDarkly if applicable
      * [ ]   SEMrush @jburton: Remove team member from SEMrush if applicable
      * [ ]   Smartling @jennyt: Remove team member from Smartling if applicable

1. [ ] IMPartner - @ecepulis @KJaeger: Remove team member from IMPartner if applicable.
1. [ ] Canva - @luke, @amittner: Remove team member from Canva if applicable
1. [ ] Xactly Incent - @lisapuzar @Swethakashyap @pravi1 @sophiehamann
1. [ ] Hopin - @khartline: Remove team member from Hopin if applicable

