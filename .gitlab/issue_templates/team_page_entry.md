name: `__FIRST_OR_PREFERRED_NAME__` `__LAST_NAME__`
locality: 
country: Remote
role: <a href="`__JOB_FAMILY_URL__`">`__JOB_TITLE__`</a>
reports_to: `__SLUG_NAME_MANAGER__`
picture: ../gitlab-logo-extra-whitespace.png
pronouns:
pronunciation:
twitter:
linkedin:
gitlab: `__GITLAB_USERNAME__`
departments:
  - `__DEPARTMENT__`
specialty: `__SPECIALTY__`
expertise:
story:
